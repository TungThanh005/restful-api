-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 13, 2018 lúc 10:51 AM
-- Phiên bản máy phục vụ: 10.1.36-MariaDB
-- Phiên bản PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `restful`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `phone`, `address`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(8, 'Nguyễn văn A', 'aaaaaaa@gmail.com', NULL, 1649671393, 'nam phu nam phong', 'Tungthanh789', '1542094074-0406anhdao.jpg', NULL, '2018-11-13 00:27:54', '2018-11-13 00:27:54'),
(9, 'Nguyễn văn B', 'bbbbbbbbbbbbbb@gmail.com', NULL, 1649671393, 'nam phu nam phong', 'Tungthanh887', '1542094095-anh-dep-3d-ve-thien-nhien-1.jpg', NULL, '2018-11-13 00:28:15', '2018-11-13 00:28:15'),
(10, 'Nguyễn văn C', 'cccccccccccccc@gmail.com', NULL, 1649671393, 'nam phu nam phong', 'Tungthanh3223', '1542094113-anh-dep-hoa-va-giot-suong-e1444384447369.jpg', NULL, '2018-11-13 00:28:33', '2018-11-13 00:28:33'),
(11, 'Nguyễn văn D', 'dddddddddd@gmail.com', NULL, 1649671393, 'nam phu nam phong', 'Tungthanh67', '1542094129-hinh-anh-dep-ve-tinh-yeu-chung-thuy.jpg', NULL, '2018-11-13 00:28:49', '2018-11-13 00:28:49'),
(13, 'Nguyễn văn F', 'fffffff@gmail.com', NULL, 1649671393, 'nam phu nam phong', 'Tung7868723e', '1542094155-images (2).jpg', NULL, '2018-11-13 00:29:15', '2018-11-13 00:29:15'),
(14, 'Nguyễn văn G', 'gggggggggggggggggggg@gmail.com', NULL, 1649671393, 'nam phu nam phong', 'Tungthasd8978', '1542094173-images.jpg', NULL, '2018-11-13 00:29:33', '2018-11-13 00:29:33'),
(15, 'Nguyễn văn H', 'hhhhhhhhhhhh@gmail.com', NULL, 1649671393, 'nam phu nam phong', 'Tjkfgvd78', '1542094191-May-Bay-Nga.Jpg2.jpg', NULL, '2018-11-13 00:29:51', '2018-11-13 00:29:51'),
(17, 'Nguyễn văn z', 'zzzzzzzzzzzzzzzzzzzz@gmail.com', NULL, 1649671393, 'nam phu nam phong', 'Tjiodfhsd79fg8s', '1542094222-tong-hop-bo-anh-3d-lam-hinh-nen-dep-nhat-song-dong-nhu-that-2-11.jpg', NULL, '2018-11-13 00:30:22', '2018-11-13 00:30:22');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
