<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
   public function show(Request $request)
   {
           $text=$request->text;
           $keyword = explode(' ', $text);
           $data = User::query();
           foreach ($keyword as $value) {
               $data->orWhere('name', 'like', '%' . $value . '%')
                   ->orWhere('address', 'like', '%' . $value . '%')
                   ->orWhere('phone', 'like', '%' . $value . '%')
                   ->orWhere('email', 'like', '%' . $value . '%');
           }
           $data = $data->distinct()->get();
           return response()->json($data);

   }

}
