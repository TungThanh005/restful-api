<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AddUserController extends Controller
{
    public function store(UserRequest $request)
    {
        $user=new User;
        $user->fill($request->all());
        $file                   =$request->avatar;
        $name                   =$file->getClientOriginalName();
        $path                   =public_path('image');
        $nameFull               = time().'-'.$name;
        $file->move($path,$nameFull);
        $user->avatar             = $nameFull;
        $user->save();
        Auth::login($user);
        return response()->json(['errors'=>false,'details'=>$user]);
    }
    public function update($id)
    {
        $user=User::FindOrfail($id);
        return response()->json($user);
    }
    public function show()
    {
        $user=User::paginate(5);
        return response()->json($user);
    }
    public function destroy(Request $request)
    {
        $id=$request->id;
        foreach ($id as $value)
        {
            $user=User::Find($value);
            unlink('image/'.$user->avatar);
            $user->delete();
        }
        $users=User::paginate(5);
        return response()->json($users);
    }

}
