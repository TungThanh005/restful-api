<?php

namespace App\Http\Requests;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
            [
                'name'              =>  'required|min:2|max:40',
                'email'             =>  'required|min:9|max:40|unique:users,email|email|regex:/^[a-z][a-z0-9_\.]{2,32}@[a-z]{3,5}(\.[a-z]{2,4}){1,2}$/',
                'password'          =>  'required|min:8|max:32|regex:/^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,32}$/',
                'phone'             =>   'required|regex:/^[0-9]{4,4}-[0-9]{3,3}-[0-9]{3,3}$/',
                'address'           =>   'required|min:9',
                'avatar'            =>   'required|mimes:jpeg,png,jpg,gif,svg,PNG,JPG,JPEG|max:2048',
            ];
    }
    public function messages()
    {
        return
            [
                'email.required'   =>'Trường này không được bỏ trống',
                'avatar.required'   =>'Trường này không được bỏ trống',
                'password.required'=>'Trường này không được bỏ trống',
                'phone.required'   =>'Trường này không được bỏ trống',
                'name.required'    =>'Trường này không được bỏ trống',
                'address.required' =>'Trường này không được bỏ trống',
                'name.min'         => 'Trường phải nhập ít nhất là 2 kí tự',
                'email.min'        => 'Trường phải nhập ít nhất là 9 kí tự',
                'password.min'     => 'Trường phải nhập ít nhất là 8 kí tự',
                'address.min'      => 'Trường phải nhập ít nhất là 9 kí tự',
                'name.max'         =>'trường nhập nhiều nhất là 40 kí tự',
                'email.max'        =>'trường nhập nhiều nhất là  40 kí tự',
                'password.max'     =>'trường nhập nhiều nhất là 32 kí tự',
                'password.regex'   =>'Mật khẩu ít nhất 8 kí tự, phải có ít nhất 1 kí tự viết hoa và có ít nhất 1 số',
                'email.unique'     =>'Tên trường đã tồn tại ',
                'email.regex'      =>'mời bạn nhập đúng địa chỉ email',
                'phone.regex'      =>'định dạng số điện thoại theo chuẩn xxxx-xxx-xxx',
                'phone.numric'     =>'hãy nhập đúng định dạng số',
                'phone.size'       =>' nhiếu nhất là 11 kí tự',
                'avatar.mimes'     =>'ảnh phải có đuôi jpeg,png,jpg,gif,svg',
                'avatar.max'       =>'ảnh đưa lên không được quá 2mb',
            ];
    }


    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([$validator->errors(),'errors'=>true]));
    }

}
