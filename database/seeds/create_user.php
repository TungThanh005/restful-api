<?php

use Illuminate\Database\Seeder;

class create_user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            for ($i=0;$i<50;$i++) {
                $user = new \App\User();
                $user->name = str_random(8);
                $user->email = str_random(9) . '@gmail.com';
                $user->password = bcrypt(str_random(6));
                $user->phone = '0989251692';
                $user->address = str_random(12);
                $user->save();
            }

    }
}
