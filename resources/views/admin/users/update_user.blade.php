@extends('admin.master')
@section('content')
    <div class="col-lg-6">
        <h1>ADD USER</h1>
        @if($errors->any())
            <div class="alert alert-danger">
                @foreach($errors ->all() as $err)
                    {!! $err !!}<br>
                @endforeach
            </div>
        @endif

        <form action="{!! route('post_sua_user',['id'=>$name->id_user]) !!}" method="post" role="form">
            @csrf
            <div class="form-group">
                <label for="disabledSelect">User Name</label>
                <input class="form-control" id="username" value="{!! $name->username !!}"  name="username" type="text" placeholder="vui lòng nhập user name ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">Email</label>
                <input class="form-control" id="email" value="{!! $name->email !!}" name="email" type="text" placeholder="vui lòng nhập mail ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">Password</label>
                @if(is_null('password'))
                <input class="form-control" id="password"  name="{!! $name->password !!}" type="password" placeholder="vui lòng nhập mật khẩu ">
                @else
                    <input class="form-control" id="password"  name="password" type="password" placeholder="vui lòng nhập mật khẩu ">
                    @endif

            </div>
            <div class="form-group">
                <label for="disabledSelect">phone</label>
                <input class="form-control" id="phone" value="{!! $name->phone !!}" name="phone" type="text" placeholder="vui lòng nhập SDT ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">address</label>
                <input class="form-control" id="address" value="{!! $name->address !!}" name="address" type="text" placeholder="vui lòng nhập địa chỉ ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">Name</label>
                <input class="form-control" id="name" value="{!! $name->name !!}" name="name" type="text" placeholder="vui lòng nhập tên của bạn ">
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" name="submit_user" value="submit">
            </div>

        </form>


    </div>
@endsection