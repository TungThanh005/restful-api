@extends('admin.master')
@section('content')
<div class="col-lg-6 alluser">
    @if(session('thongbao'))
        <div class="alert alert-success">{!! session('thongbao') !!}</div>
    @endif

    <h1>Thêm mới người dùng</h1>
        <h4 style="color: #0d6aad" class="errors success1"></h4>
    <form action="" method="" role="form" enctype="multipart/form-data">

        <div class="form-group">
                <label for="disabledSelect">User Name</label>
                <input class="form-control" id="name" value="{!! old('username') !!}" name="name" type="text" placeholder="vui lòng nhập user name ">

                    <span class="text-danger errorsname errors"></span>
            </div>
            <div class="form-group">
                <label for="disabledSelect">Email</label>
                <input class="form-control" id="email" value="{!! old('email') !!}" name="email" type="text" placeholder="vui lòng nhập mail ">
                <span class="text-danger errorsemail errors"></span>
            </div>

            <div class="form-group">
                <label for="disabledSelect">Password</label>
                <input class="form-control" id="password"  name="password" type="password" placeholder="vui lòng nhập mật khẩu ">
                <span class="text-danger errorspassword errors"></span>
            </div>

            <div class="form-group">
                <label for="disabledSelect">phone</label>
                <input class="form-control" id="phone"  value="{!! old('phone') !!}" name="phone" type="text" placeholder="vui lòng nhập SDT ">
                <span class="text-danger errorsphone errors"></span>
            </div>
            <div class="form-group">
                <label for="disabledSelect">address</label>
                <input class="form-control" id="address" value="{!! old('address') !!}" name="address" type="text" placeholder="vui lòng nhập địa chỉ ">
                <span class="text-danger errorsaddress errors"></span>
            </div>
        <div class="form-group">
            <label for="disabledSelect">avatar</label>
            <input class="form-control" id="avatar" value="{!! old('avatar') !!}" name="avatar" type="file"  >
            <span class="text-danger errorsavatar errors"></span>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary " id="submit" name="submit_user" value="submit">

        </div>
    </form>
</div>
@endsection