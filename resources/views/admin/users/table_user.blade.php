@extends('admin.master')
@section('content')
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading h1-1">
           <h1>Danh Sách User</h1>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>phone</th>
                        <th>address</th>
                        <th>Name</th>
                        <th>Ngày tạo</th>
                        <th>Ngày sủa</th>
                        <th>active</th>
                        <th>chức vụ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($kq as $key => $value)
                    <tr>
                        <td>{!! ((($page * $limit)-$limit)+1)+$key !!}</td>
                        <td>{!! $value->username !!}</td>
                        <td>{!! $value-> email!!}</td>
                        <td>{!! !is_null($value->phoneNumber)? $value->phoneNumber->phone_number : '' !!}</td>
                        <td>{!! $value-> address!!}</td>
                        <td>{!! $value->name !!}</td>
                        <td>{!! $value-> created_at!!}</td>
                        <td>{!! $value-> updated_at!!}</td>
                        <td>
                            <a href="{!! route('get_sua_user',['id'=>$value->id_user]) !!}" CLASS="glyphicon glyphicon-edit"></a>
                            <a href="javascript:void(0)" onclick="delete_user({!! $value->id_user !!})" CLASS="glyphicon glyphicon-trash"></a>
                        </td>
                        <td>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    <div class="pagination">
        @if($page > 1 && $total_page > 1)
            <a class="btn btn-primary" href="{!! route('danhsach_user',['page'=>$page-1]) !!}" active>prev</a>
           {{-- @for($j=1; $j <= $total_page ; $j++)
                        <a href="">{!! $j+1 !!}.ssds</a>
                    <a href="">{!! $j+1 !!}.ssds</a>
                    <a href="">{!! $j+1 !!}.ssds</a>
                ......
                    <a href="">{!! $total_page-2 !!}.ssds</a>
                    <a href="">{!! $total_page-1 !!}.ssds</a>

                 @endfor--}}
            @endif
         @for($i=1; $i <= $total_page ; $i++)
             @if($i==$page)
                    <a class="btn btn-danger" style=" pointer-events: none;color: #ccc; color: red" href="">{!! $i !!}</a>
                {{-- @for($j=1; $j <= $total_page ; $j++)
                          @if()
                            <a href=""></a>
                              @endif
                     @endfor--}}
             @endif
             @if($i != $page)
                     <a class="btn btn-primary" href="{!! route('danhsach_user',['page'=>$i]) !!}">{!! $i !!}</a>
             @endif

             @endfor
        @if($page >0 && $page<$total_page)
                <a class="btn btn-primary" href="{!! route('danhsach_user',['page'=>$page+1]) !!}" active>next</a>
            @endif
    </div>
@endsection
@section('script')
    <script>

            function delete_user(id) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                check=confirm('ban co chac la se xoa khong?');
                if (check){
                   $.ajax({
                        'url':'{!! route('delete_user') !!}',
                         'type':'POST',
                           'datatype':{'id':id},
                          success:function (data) {
                              alert(data);
                          }
                   });
                }
            }

    </script>

    
    @endsection
