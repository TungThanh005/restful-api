@extends('admin.master')
@section('content')
    <div class="col-lg-6">
        <h1>ADD POST</h1>

        <form method="post" action="{!! route('post_sua_posts',['id'=>$name->id_posts]) !!}" role="form">
            @csrf

            <div class="form-group">
                <label for="disabledSelect">Title</label>
                <input class="form-control" id="title"  name="title" value="{!! $name->title !!}" type="text" placeholder="vui lòng nhập title ">
            </div>

            <div class="form-group">
                <label for="disabledSelect">Description</label>
                <input class="form-control" id="description"  name="description" value="{!! $name['description'] !!}" type="text" placeholder="vui lòng nhập Description ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">Content</label>
                <input class="form-control" id="content"  name="content" value="{!! $name['content'] !!}" type="text" placeholder="vui lòng nhập content ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">View</label>
                <input class="form-control" id="view"  name="view" value="{!! $name['view'] !!}" type="text" placeholder="vui lòng nhập view ">
            </div>
           {{-- <div class="form-group">
                <label for="disabledSelect">User_id</label>
                <input class="form-control" id="user_id"  name="user_id" value="{!! $value['user_id'] !!}" type="text" placeholder="vui lòng nhập view ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">category_id</label>
                <input class="form-control" id="category_id"  name="category_id" value="{!! $value['category_id'] !!}" type="text" placeholder="vui lòng nhập view ">
            </div>--}}
            <div class="form-group">
                <label for="disabledSelect">User_id</label>
                <select id="user_id" name="user_id" class="form-control">
                    @foreach($user as $key =>$value)

                    <option
                            @if($value->id_user==$name->user_id)
                            {!! 'selected' !!}
                            @endif
                            value="{!! $value->id_user !!}">{!! $value->username !!}</option>


                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="disabledSelect">Category_id</label>
                <select id="category_id" name="category_id" class="form-control">
                    @foreach($name1 as $key => $value)
                    <option
                            @if($value->id_category == $name['category_id'])
                                    {!! 'selected' !!}

                            @endif
                            value="{!! $value->id_category !!}">{!! $value->name !!}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
            <input type="submit" class="btn btn-primary" name="submit_post" value="submit">

    </div>

    </form>
    </div>


@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#user_id').change(function () {
                var id=$(this).val();
                alert(id);
                $.get("ajax_post/"+id,function (data) {
                    alert(data);
                    alert('23456');
                    /*$('#category_id').html(data);*/

                });

            });

        });
    </script>
    @endsection