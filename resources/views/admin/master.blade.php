
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>
    <link href="{!! asset('bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/sb-admin-2.css') !!}" rel="stylesheet">

    <link href="{!! asset('font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{!! asset('css/my.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/css.css') !!}">
</head>

<body>

    <div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        @include('admin.layout.header')
        <!-- /.navbar-header -->

        @include('admin.layout.header1')
        <!-- /.navbar-top-links -->

      @include('admin.layout.sidebar')
        <!-- /.navbar-static-side -->
    </nav>
    <div id="page-wrapper">
        <div class="row">
       @yield('content')

        <!-- /.row -->

        <!-- /.row -->
        </div>
    </div>

    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="{!! asset('jquery/jquery.min.js') !!}"></script>

    <script src="{!! asset('js/my.js') !!}"></script>


<script>
    @if(Session::has('thongbao'))
        alert("{!! Session::get('thongbao') !!}")

    @endif
</script>
@yield('script')

</body>

</html>
