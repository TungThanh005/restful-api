@extends('admin.master')
@section('content')
    <div class="col-lg-6">


        <div class="panel panel-default">

            <div class="panel-heading h1-1">
                <h1>Danh Sách category</h1>

            </div>
            @if(session('thongbao'))
                <div class="alert alert-success">
                    {!! session('thongbao') !!}
                </div>
        @endif
         @if(session('baocao'))
             <div class="alert alert-success">
                 {!! session('baocao') !!}
             </div>
         @endif
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Ngày tạo</th>
                            <th>Ngày sủa</th>
                            <th>active</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($name as $key => $value)


                        <tr>
                            <td>{!! $key +1 !!}</td>
                            <td>{!! $value->name !!}</td>
                            <td>{!! $value->created_at !!}</td>
                            <td>{!! $value->updated_at !!}</td>
                            <td>
                                <a href="{!! route('get_sua_cate', ['id' => $value->id_category]) !!}" CLASS="glyphicon glyphicon-edit"></a>
                                <a href="javascript:void(0)" onclick="delete_cate({!! $value->id_category !!})" CLASS="glyphicon glyphicon-trash"></a>
                            </td>

                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
@endsection
@section('script')
    <script>
        function delete_cate(id) {
          check=confirm('ban co chac chan xoa khong?');
          if (check){
              window.location.href='{!! route('delete_cate') !!}'+'/'+id;
          }

        }
    </script>
    @endsection