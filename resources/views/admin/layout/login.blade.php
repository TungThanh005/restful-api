<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="{!! asset('vendor/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{!! asset('vendor/metisMenu/metisMenu.min.css') !!}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{!! asset('dist/css/sb-admin-2.css') !!}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{!! asset('vendor/morrisjs/morris.css') !!}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{!! asset('vendor/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">
    {{--link rel="stylesheet" href="{!! asset('css/css.css') !!}">--}}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>


    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <![endif]-->

</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    @if(Session::has('thongbao'))
                        <div class="alert alert-danger">
                            {!! Session::get('thongbao') !!}
                        </div>
                    @endif

                    <h3 class="panel-title">Please Sign In</h3>
                </div>
                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                            {!! $err !!}<br>
                         @endforeach
                    </div>
                @endif
                <div class="panel-body">
                    <form role="form" action="{!! route('post_login') !!} " method="post">
                        @csrf
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="E-mail" name="email" type="text" autofocus value="{!! old('emai') !!}">
                            </div>
                            @if($errors->has('email'))
                                <span class="text-danger">{!! $errors->first('email') !!}</span>
                            @endif
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                            @if($errors->has('password'))
                                <span class="text-danger">{!! $errors->first('password') !!}</span>
                            @endif
                            <div class="checkbox">
                                <label>
                                    <input name="remember" type="checkbox" value="1">Remember Me
                                </label>
                            </div>
                            <a href="{{ route('redirectFb') }}"> FBlogin</a>
                            <!-- Change this to a button or input when using this as a form -->
                            {{--<a href="index.html" class="btn btn-lg btn-success btn-block">Login</a>--}}
                            <input type="submit" value="Login" class="btn btn-lg btn-success btn-block">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="{!! asset('vendor/jquery/jquery.min.js') !!}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{!! asset('vendor/bootstrap/js/bootstrap.min.js') !!}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{!! asset('vendor/metisMenu/metisMenu.min.js') !!}"></script>

<!-- Custom Theme JavaScript -->
<script src="{!! asset('dist/js/sb-admin-2.js') !!}"></script>

</body>

</html>
