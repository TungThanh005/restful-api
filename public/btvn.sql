-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 12, 2018 lúc 06:26 AM
-- Phiên bản máy phục vụ: 10.1.33-MariaDB
-- Phiên bản PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `btvn`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id_category`, `name`, `created_at`, `updated_at`) VALUES
(3, '12345', NULL, '2018-08-01 09:00:24'),
(4, '3DkP1', NULL, '2018-08-01 09:20:27'),
(5, '789', NULL, '2018-08-01 09:20:56'),
(6, 'tungthanh', '2018-07-22 10:12:02', '2018-07-22 10:12:02'),
(8, 'tungthanh12', '2018-07-22 10:22:15', '2018-07-22 10:22:15'),
(9, '121212121', '2018-07-25 03:31:29', '2018-07-25 03:31:29'),
(10, '121212121', '2018-07-25 03:32:13', '2018-07-25 03:32:13'),
(11, '121212121', '2018-07-25 03:32:27', '2018-07-25 03:32:27'),
(12, '121212121', '2018-07-25 03:32:56', '2018-07-25 03:32:56'),
(13, 'new', '2018-07-25 03:43:43', '2018-07-25 03:43:43'),
(14, 'haha', '2018-08-01 07:43:47', '2018-08-01 09:27:06'),
(16, 'werturew', '2018-08-01 07:44:30', '2018-08-01 07:44:30'),
(17, '1211', '2018-08-01 07:45:18', '2018-08-01 07:45:18'),
(18, '14521', '2018-08-01 07:47:04', '2018-08-01 07:47:04'),
(19, '25469', '2018-08-01 07:47:45', '2018-08-01 07:47:45'),
(20, 'asdg', '2018-08-01 09:29:56', '2018-08-01 09:29:56'),
(21, '1234567984', '2018-08-01 12:12:43', '2018-08-01 12:12:43'),
(22, 'hyhhy', '2018-08-01 13:33:05', '2018-08-01 13:33:05'),
(23, 'fgfdgdgdgd', '2018-08-01 13:33:31', '2018-08-01 13:33:31'),
(24, 'hsdhjdasd', '2018-08-01 13:45:33', '2018-08-01 13:45:33'),
(25, '123232', '2018-08-01 13:46:54', '2018-08-01 13:46:54');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_21_140954_tao_cate', 2),
(4, '2018_07_21_144814_add_post', 3),
(5, '2018_07_22_172433_add_new', 4),
(6, '2018_07_22_173357_add_pass', 4),
(7, '2018_07_22_173816_del_colum_pas', 5),
(8, '2018_07_22_174105_del_addnew', 6),
(9, '2018_07_22_180510_rename_pas', 7),
(10, '2018_07_22_181133_add_name_pass', 8),
(11, '2018_08_06_185901_add_column_type_user', 9),
(12, '2018_08_06_194519_create-phone-user', 10),
(13, '2018_08_08_193822_create-role', 11),
(14, '2018_08_08_193851_create-rol-user', 11);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password`
--

CREATE TABLE `password` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `phone`
--

CREATE TABLE `phone` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone_number` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `phone`
--

INSERT INTO `phone` (`id`, `phone_number`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '212121212122', 4, NULL, NULL),
(2, '2345443565432532', 9, NULL, NULL),
(3, '0989251692', 15, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id_posts` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id_posts`, `title`, `description`, `content`, `view`, `user_id`, `category_id`, `created_at`, `updated_at`) VALUES
(13, '12345678', '123456iop', '123456', 123456789, 4, 4, '2018-07-23 17:50:46', '2018-07-23 17:50:46'),
(14, 'new23', '32323232', '3232323', 1, 4, 4, '2018-07-25 08:33:18', '2018-07-25 08:33:18'),
(15, 'new23', '32323232', '3232323', 1, 4, 4, '2018-07-25 08:36:44', '2018-07-25 08:36:44'),
(19, '21212', '21212', '12121', 212, 9, 11, '2018-07-29 16:56:05', '2018-07-29 16:56:05'),
(22, '1ertyh', 'wertj', 'asdfgh', 11, 4, 8, '2018-07-30 12:26:56', '2018-07-30 12:26:56'),
(24, 'ưeewer', 'qwe', 'wef', 1, 4, 12, '2018-08-01 02:52:34', '2018-08-01 02:52:34');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role`
--

CREATE TABLE `role` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role`
--

INSERT INTO `role` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'giám đốc', NULL, NULL),
(2, 'nhân viên', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 4, 1, NULL, NULL),
(2, 4, 2, NULL, NULL),
(3, 9, 2, NULL, NULL),
(4, 15, 1, NULL, NULL),
(5, 16, 1, NULL, NULL),
(6, 15, 2, NULL, NULL),
(7, 16, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quyen` int(1) NOT NULL,
  `phone` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id_user`, `username`, `email`, `password`, `quyen`, `phone`, `avatar`, `address`, `name`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'new1', 'tungthanhnp1@gmail.com', '$2y$10$CxjTooUber5beKMhSa7mPeI5MCO91iLVs6J7FTSp5.dZot3Lnuw0i', 0, '65456564', '', 'nam phu1', 'tung1', '', NULL, '2018-08-06 07:31:36'),
(9, 'b5ETUjldtO1', 'wnOAXirLoKYC@gmail.com', '$2y$10$RA9HGu2Qt.QTFNcnB8YRz.qSo0O2NkulEO4VCrtFdHsVkU.c2Ty4O', 0, '01649671393', '', 'TmJiw5GNqfVHbWhZXzfm', 'l8lzWmd2DX', '', NULL, '2018-09-10 10:04:58'),
(15, 'user11', '2121212', '$2y$10$uhyOAk2Og5Wh9mWqfVob.uO1CEbfUZyGDn4G1Db4i9kXVn0FuOFhC', 0, '2121212', '', '121212', '121212', '', '2018-07-25 08:45:40', '2018-07-25 08:45:40'),
(16, 'tung1221132', 'tungthanh2np@gmail.com', '$2y$10$V44nNYxjaPk3JSHPVHsrLOqWR.9ocZHdGSmC7dUhzOmcbpSog5ccy', 0, '01649671393', '', 'nam phu-nam phong-phu xuyen-ha noi', 'wewewe', '', '2018-08-03 06:59:29', '2018-08-03 06:59:29'),
(17, 'tung121212121', 'tungth212anhnp@gmail.com', '$2y$10$qnsy4vlaojWdE8oVEc0zL.XCYlXsMBuiAD492ohY2pOGI15QcAaY2', 0, '01649671393', '', 'nam phu-nam phong-phu xuyen-ha noi', 'tungthanhnp@gmail.com', '', '2018-08-03 07:03:56', '2018-08-03 07:03:56'),
(18, 'tung1212121', 'tungthan222hnp@gmail.com', '$2y$10$BYsjHqCLgSMQlWbTof7iquY78u0EdIpgDDkV8N2z2lTOZo/Juq4Q6', 0, '0989251692', '', 'nam phu-nam phong-phu xuyen-ha noi', 'thanh tung', '', '2018-08-03 07:04:41', '2018-08-03 07:04:41'),
(19, 'tung12121', 'tungthanh22222np@gmail.com', '$2y$10$sl9W7/cwLtxHgqI7uyYlBeIlL1R.F/gJlvWA4zmWAVDn2w5drRcGm', 0, '01649671393', '', 'nam phu-nam phong-phu xuyen-ha noi', 'wewewe', '', '2018-08-03 07:05:44', '2018-08-03 07:05:44'),
(20, 'sdsd11212', '12121212@gmail.com', '$2y$10$TUGgQp0Q6mqfFusaQY9WGuEqAAadxPIGCI8NI8DIt.1U/83wKbEl6', 0, '0989251692', '', 'nam phu-nam phong-phu xuyen-ha noi', 'wewewe', '', '2018-08-03 07:07:24', '2018-08-03 07:07:24'),
(21, 'tung12121211111', 'tungth22222222anhnp@gmail.com', '$2y$10$2jDcy5cSq5gWIe8ldZc1LOFj7NGPgkS7q23A2s/ULHqHT.1.eNG9.', 0, '0989251692', '', 'nam phu-nam phong-phu xuyen-ha noi', 'wewewe', '', '2018-08-03 07:08:08', '2018-08-03 07:08:08'),
(22, 'tung11123467890-', '12345@gmail.com', '$2y$10$ySx7HQfVmdoKM4df9Ofc0eMpF9N4GW8Ma445h2Wk/aQ5rojOk.tnC', 0, '01649671393', '', 'nam phu', 'thanh tung', '', '2018-08-03 07:09:11', '2018-08-03 07:09:11'),
(23, 'sdsd12121212', '12121212222@gmail.com', '$2y$10$czU7cKKIm49Y7d/7inlksu9gNSrZg0.byfMlukNoMEMAbyhwjWqGC', 0, '01649671393', '', 'nam phu', 'thanh tung', '', '2018-08-03 07:10:18', '2018-08-03 07:10:18'),
(24, 'admin', 'tungthanhnp@gmail.com', '$2y$10$5oa4irpk9FX5.gMpQdGAueeqGGHTXJg6ZmS0s4NHbshgnnjkj1ugW', 1, '0989251692', '', 'nam phu-nam phong-phu xuyen-ha noi', 'tungthanh', 'dFt7remd3qIosYSXnqwMrsPIC9VNhmrH2VYtHAMrMgB0Dk9hT7cQsx7J58Tf', '2018-08-06 07:16:24', '2018-08-06 07:16:24'),
(25, 'admin1', 'tungthanhnp11@gmail.com', '$2y$10$mHZuG77Iy1H.0xiLAaABs.ZYD4MstV4Q2mrNW5Xwl4a9BbeSg8blO', 1, '0989251692', '', 'nam phu-nam phong-phu xuyen-ha noi', 'wewewe', '', '2018-08-06 08:43:33', '2018-08-06 08:43:33'),
(26, 'admin2', 'tungthanhnp22@gmail.com', '$2y$10$2wX.EzXMoPyUYVi2Aqj1teHuL1.7jjCMN8dl6zvk/CpBC/.cYjfJu', 1, '0989251692', '', 'nam phu', 'wewewe', '6K2AG3oTOL0dVjFVIpq7xYvS0GJfwXFaYfM4Jc4U', '2018-08-06 08:54:30', '2018-08-06 08:54:30'),
(27, 'tungthanh', 'tungthanhnffffffp@gmail.com', '$2y$10$iAq9dslYEVv3TgUwaHNTju8X.jLOER2FHrJBpdAS5Pn8nhEjFs4Sy', 0, '01649371393', 'showpass1536565608-png', 'nam phu', 'tung', 'HfNYYmnamlqeVHwytHFlr49Npven3VxxtQGaiiCk', '2018-09-10 07:46:48', '2018-09-10 07:46:48'),
(28, '7AG3tLfFPF', 'dGQe5qCybi05', '$2y$10$Wx1J4VsXSddT0UJr70KTleqOcamh8zLvsotkTTHlaeseETxyjzRs2', 1, '01649671393', 'bnwiBgLIk1jpg', 'vMQupC37ev1tsDzVulMj', 'RqkUEIzKkn', 'ugxcmN3cPFbuUKMmNUX5nwcbvPQS6H', NULL, NULL),
(29, 'zKi157OSBI', '7IWCpgFf7EwG', '$2y$10$n.0TOwbPyMr3ry/W2O2ZmuYhYNFL7ts.92/U5t7T5oN.WYhBqim5.', 1, '01649671393', 'K0R2UcUASQjpg', 'lxWKtNQpaUBiCQarNMHA', 'Bu6JOh6JEP', 'VGGHH8Ps1aBTOOYXYxPWq8FkXyarnT', NULL, NULL),
(30, 'FOn1OJRLyx', 'oCMImuiIBE8P', '$2y$10$zbkX/y2iHRN0d9GlC3j1duyFXikarTo1TdO97hsc4fjFY2h25z9eO', 1, '01649671393', 'hQIvqnEP1Zjpg', 'vqpRzkrOvh0JttbnxOss', '2G3q2oc1rb', 'IbL7S6d6K500P2gLSrzbXU1G1vzOpm', NULL, NULL),
(31, '1wzHDCBsXH', 'A58Mm0oWWXi4', '$2y$10$x/z0VdlHsMV5grvbJrAzGuNn44JznDp77.XQ5eCdZa3sbsxtptmrW', 1, '01649671393', 'UldCi8DQRMjpg', '7TqWgzi30G8CSDTkLk3L', '', 'XMXc6kUXK3AwokLtMHHcEQBf8pvvWm', NULL, NULL),
(32, 'Or2Bk4IMki', 'khKnscAvHshX', '$2y$10$YB/zjBVuf7MzWYD8dICRHO5YjJSiXJUzUmdHxDSygrqoXgyG1tccK', 1, '01649671393', 'SxWVU3S3dLjpg', 'q93cf3xOc5ZPcHfnhdHv', 'emXn6NHX4F', 'o93cbEtEYd5xPXDl0gCB9y0dTIarzt', NULL, NULL),
(33, 'RF8I9Bl76L', 'gQNN1dWC7O6q', '$2y$10$Xq5vbm2iIF2P4M6PcKbYiepEYpwc2uGd1revqmALqIYxdkj6.J5Le', 1, '01649671393', '9unTIfgE9Djpg', '0XRnFcYK1NZVwBA4MIUE', 'qvAWfoFFBc', 'ha52SaPzVJAzXqI5PRGP5zxgNif2he', NULL, NULL),
(34, 'JrWP8LHSvS', 'GzYLLC6rRLyj', '$2y$10$fFnzB9/xHwUVp9gRwfGzdOg0NhwPYYOgwwRcclnZy9vu.akUaat8W', 1, '01649671393', 'R5AdrM5T5fjpg', 'FxdhTaWaxaQldpvwjf36', 'VsLnU2nRWw', 'fIjDhTsiwlL5CPgpW20IOegyrRbyVd', NULL, NULL),
(35, 'WtF7AXZMzb', 'On1mHSitgDgx', '$2y$10$kEmJTFx2YJzoL9Cdz/HCE.ptwFbiXgUS5tRtsNGj9lmZHw4tcpDJO', 1, '01649671393', 'LZozctA9C8jpg', 'ysSYbgsdB3e2rEOxsGe6', 'j4xC9CkvBK', '7Qn62WaqB3eqXY6U1gVEJec49qVvtB', NULL, NULL),
(36, 'tdyDTCY6sE', 'QlWTPVyeqTFJ', '$2y$10$EHaNmL9MD8kUqQ05gIPXv.zXgVCShhUdu5kKxfEZoN7MZGy.VErpe', 1, '01649671393', 'qi6dirFR9ijpg', 'yg7PN7FECUbdOifzVMyO', 'EX2DFQeV26', 'xaQJwiyYajGC74npILuWtmj4epZNFA', NULL, NULL),
(37, 'VtmlAEHrRm', '7FvAkTAGoxeM', '$2y$10$MjZe0sT7FlIJpz/5IkF...uWSqvuOjypDIiZkI2TCNIBLkCX2FBTm', 1, '01649671393', 'Moo6IDzcPEjpg', 'jAr5RTgXP2qSUb5aRI4l', 'pD69EjS6yt', '0pDuF7KuRglYxnvpI49CAMkZspp7bV', NULL, NULL),
(38, 'OKTrDE8gaj', 'sm8KM3DAgwwt@gmail.com', '$2y$10$XQx7viln.sR2jFpQsvgZ2eQmD/2dlddd6dgXCTjjKH5joNLoG9yI6', 1, '01649671393', 'XC8hXcY76Jjpg', 'e9AXgLWgvJF7okpHtliq', 'uv7fi9xcV0', 'oVRN7RotSjnhQiSVG9nlz2JJCiAqtY', NULL, NULL),
(39, 'SUo8sRdobv', '7tBKfovkJWqH@gmail.com', '$2y$10$AAlNB0bwg8lHBL9t3j7qZet1r.hy/HVfYn2w6z8MvC3NO72p/NXoe', 1, '01649671393', 'CDsQvjnFS3jpg', 'GRJfOmAHSubGhtvaZhSm', 'o3kMjTJOMf', 'EDTNiBmWMzzqaFqlUYGfco6TYkVptx', NULL, NULL),
(40, '14Q8PD4188', 'xqre34p9HWxL@gmail.com', '$2y$10$uP0dao9Y8t5v.fm89JEqb.JA.qH7Ygki8XE6ukBNskS3QzrsNBx22', 1, '01649671393', '0pMu9b9mZNjpg', 'w6pp7V5MetEUIfYiDHEq', 'Gjje31gkrf', 'VTGo51N1oRs1ZmtntKHqjRGHVy95Uc', NULL, NULL),
(41, 'BJBByLBUp1', 'Lr7UZgn9zo4x@gmail.com', '$2y$10$r.EyNRg0qp6zgoNITeFaI.9KVx.10DrI79w.pmnczc1Vd6CkumjWi', 1, '01649671393', 'qvpk8cp3Vijpg', 'af7KeW1jPOHcmP9GH7mY', 'NtVGvjXBvE', 'zJszkMmufNtZr3fZiV9GPbuRdrtbXk', NULL, NULL),
(42, '84z7CzKieU', '34MW1T2rrHPM@gmail.com', '$2y$10$onproNwPEJslUzw/nsHN7elP.yUOVC2HwWDVdpX28mh3VKO5sVLr.', 1, '01649671393', 'oPjJsfcF2ijpg', 'y6emVFIoHMJXeNSiVB6W', '', 'XUs73c2RsBAFnZPr9mmKi1nZOfR9ki', NULL, NULL),
(43, 'GD0BCdvOs4', 'CJYUsWvK6yzT@gmail.com', '$2y$10$ePEqysiYiuQcjZtZFrn9heTy4q3I/6jnSyPZfUl8BkQkpY5dgFJ4K', 1, '01649671393', 'tCYHWA2R3hjpg', 'AEPV933BlbT49SuHTA3k', 'GhiC3uiXy8', 'GhP8305PYKYn13ssGtiiOsnpfi7wSh', NULL, NULL),
(44, 'tSTj8bAD6Q', '4CqeIpKjlMdj@gmail.com', '$2y$10$6m2eALqoB8WiA.h3Cl/dx..FWesWoOK2Mwhekge24EhzAP.cQ2FeO', 1, '01649671393', 'uRpt7QNnsSjpg', 'iGywopuyjeKoigIWgwOt', 'AunqCACR8f', 'Z4n3NVmajHh1k5OuN7NrJcLAw2qdhF', NULL, NULL),
(45, '6IDHMiT18V', '7TfSm5Hyk7pR@gmail.com', '$2y$10$PUQdQrwWs6cbkR6BRP//heOgs9TVShqSCYSHRJunWyw/ln2gHsBlW', 1, '01649671393', 'Uh70DMVwrgjpg', 'jJk7LZroWHelLXJItp2i', 'Wln91f2nqK', 'K4sb1V3U4NVR3gmnCRyFx95jhi01Al', NULL, NULL),
(46, '9STjjODslA', '5AoD86WkuqgS@gmail.com', '$2y$10$RI3GrcmvomZ89Gamahctf.YQZmJC29x9LjYO48Z3mkeLEBRXWTXa2', 1, '01649671393', 'KJXb21iyD1jpg', 'AYRBWEIOurVsCQjImNKk', '0UflxI803r', 'avYmyOG7Ih0NJPxewvFvvx6H5BUP2g', NULL, NULL),
(47, '27laH6g9zy', 'U9LT27Mz9L2U@gmail.com', '$2y$10$ffjbAfmtaaJCZk41/F0LguCBtyvOzBOLNyTPGh7XhTkM2/CaIhlJm', 1, '01649671393', 'xjjQPeNJgCjpg', '80jyKNcRkUOlXBvxEn2T', 'B7nx7RQLno', 'S6fHuSw3aC9HggFOZ20mytDh69iMv9', NULL, NULL),
(48, 'wkE3i9xYEi', 'KhesySdxxvkq@gmail.com', '$2y$10$OBOUcuPyV56O5WSqVaHEWe3d34InJ0OioEyIiH/6VcboqmUJoNYt6', 1, '01649671393', 'YZXtv7NZhCjpg', 'lMFy66Fon70fhug9YqDz', '8Syv5u9IkR', 'XhKa0MjY7vE909MIVBtnPl4p7F4B97', NULL, NULL),
(49, 'GGcAWngWvy', 'mNKDC6hzqGox@gmail.com', '$2y$10$w6p2VRPVQUPtBtE2BWLOSufBwIt1tYKFDEF./iYdYiDRNGYXVj/a.', 1, '01649671393', 'kSSoyJF9Sujpg', 'wd5AzXtSgZGJ3GycMxFF', 'ddD2JViMkC', 'f1pDWgBTX5cZKPjlmtGv8PWfeGi7RF', NULL, NULL),
(50, 'piSViYeWdf', 'K2STJ9qRleWy@gmail.com', '$2y$10$6H2Q3dj9P29buT6kkVeVfOC9SPP1URsFJV8.wovtQDHS7IxMBeL2u', 1, '01649671393', 'vowqWdzmWhjpg', 'BI41oIIBQyFRm0xxZigT', 'uwKuVKLWgr', 'ANe6bQ0AcSD7h7gHYljZmhIc8xjD92', NULL, NULL),
(51, 'O8r3WYjEXM', 'KK7aj7lhXMIO@gmail.com', '$2y$10$AAbMLpNG9B2OYfSE27D.N.YAAraWsJwdi0LMw2VXrIvCahlt.INpy', 1, '01649671393', 'v3x47r89kTjpg', 'N9Ne8fp0Wxv5K8vqoJus', 'IG4H9RTaOQ', 'WUIvBh28ducSJ8PuhsmrIyKUfnT9lS', NULL, NULL),
(52, 'ALrwumYphP', 'ulOIS5BtB6v5@gmail.com', '$2y$10$KQ5WEDf1CTKee7goqJR7S.n3fT0SB.XKIM9TFCy4PrKT0pHliA.hS', 1, '01649671393', 'dnfNPigQbFjpg', 's0Y3vT6HkUbdUh27qBK9', '5sixLiMPCq', 'wWxSEvI5f1bmXAP3ji8A6NMTJBnYpZ', NULL, NULL),
(53, 'u0JqEyG5Nb', 'mharFZRUI4Oj@gmail.com', '$2y$10$JEq5dp4R6mmA4MCLzHQUgeOj4xjXsW26rXVzETPRDNA2ax.6xP1JS', 1, '01649671393', 'P1TFZ6f9majpg', '0NkDtYpZGiAcGpqVZXTn', 'jxk9DiRNLE', '2djraKy036FGd7ECJUFrwlgAiuNhmU', NULL, NULL),
(54, 'frVwXKRvux', '91gncoflC2Rg@gmail.com', '$2y$10$sTsx2W0va8xjP9Mi0DLF.u4SKWb07Yt6ZpNwRvrlZxJ0acXdUkgiq', 1, '01649671393', 'Bd0wLYfF8Zjpg', 'hoAYbghOr1tnlQMmxUGo', 'sSTDHp0vjJ', 'GQPV7lRhziN7eYuYTWhcGUaDEz86VM', NULL, NULL),
(55, 'NesqaLCzlh', 'jOUpnXs7P1Mk@gmail.com', '$2y$10$brRIHfIeYLMQ2thjUoEzBOIGi4fSAgjpcXZuVochNEtlOdcpDXsMu', 1, '01649671393', '5LQ79NDMNKjpg', 'WXf7iqOQstSCHxTSiRBn', 'xDuRNqCmTf', 'LPy0ETFnRMTab7qEkPwbNztBj7y64A', NULL, NULL),
(56, 'aDw36eOGkc', 'gk9HDugSEn6M@gmail.com', '$2y$10$ogclg3UQVw2MBZ6RIyfseOR5JW3duciRcaztZ2/8Z9jnzJiAaH34S', 1, '01649671393', 'M6i7SLFukNjpg', 'M10dZGRnmXSQaBmDJaGy', 'u5VdpgM3Ju', 'QoRgU1KdtzQUQHRObmHPJzbwoZS70I', NULL, NULL),
(57, 'ccUXW00xoZ', 'jJQX0FSajifa@gmail.com', '$2y$10$2evYyZhzip3QSlkY5A9KF.yx9f46T0HeDbqfxvH8RNjUM28ncRElq', 1, '01649671393', 'wPG0AS2eoajpg', 'mbuiWwx6sg0u75KQERYK', 'XyVJq9PN8I', 'IXmXKEf0tgr2fCFfVu49nnLOpRhr2S', NULL, NULL),
(58, '0HNFIMl3L2', 'NwdWUu0mwGWZ@gmail.com', '$2y$10$P.QMnIHZCXryWgg9Ce8fneglHo1jca.aNcBdPu3Ap3PjSwVueaepu', 1, '01649671393', 'rU4wEffug1jpg', 'CjhBFo3SIFB1ViGBWXYB', 'FnG9ABcrBL', '7pgdTuWzzmrxPAPDL1tIC4uDZzpRjj', NULL, NULL),
(59, 'OkKXmR3B1e', 'kUPreENlbUHu@gmail.com', '$2y$10$DDSevgS216AmrC8jdV.NC.zJ9p56MsqwWWUf1R3uMxqUMg2V.x86i', 1, '01649671393', 'rJSpLAWgoMjpg', 'BGED0tbtxNb0WXyLGIHx', 'UcQnZiGOTd', 'OmUdjYaux5249Kv9p8MOp8AEbedn6z', NULL, NULL),
(60, 'yD64dEs2qx', 'hCtv99cU3Ppo@gmail.com', '$2y$10$6lARfvipf.ya4kFtjaWnNuTK6NuNd/u6Z50KNdBwirEqkpJcZTjwG', 1, '01649671393', 'GRaFf0086Ujpg', 'zsTtD0YGftCQ9nsJLlIw', 'aScRag2d4y', 'aVTN6t8VjIDHeapOCUzSnk7NNp94vg', NULL, NULL),
(61, 'AT9jBFXtYd', 'ySJkpxFg91AQ@gmail.com', '$2y$10$1eAq40eHthnILKVDKagvmOVrUDDglqFW90ra.oSc7n63AvBD7xHFO', 1, '01649671393', '3DsZoPrzuYjpg', 'PAHzZhlRfTFpCxJn9WTC', 'YClA1xcKi7', 'MdP5vBUEI0Tz1tcL1dTxIB3GRTT2sm', NULL, NULL),
(62, 'tVlKLrvmv5', '1pzN18FUw4hk@gmail.com', '$2y$10$jepE7DwNhdwAsIIPgKROl.iIZSe5K32luZVltYNC8twsxV6mBmXai', 1, '01649671393', 'lIq91gT6iMjpg', '66VWmKidls0lfjtMOc1U', '1CmbYn3pMS', 'X3rW6Jt2J1Ybt7x1cgvV0qJ9lLW1W7', NULL, NULL),
(63, '5SPvHbQ9Tb', 'HqnJA5JtBXWl@gmail.com', '$2y$10$cZWCviXef1Lz/Mxze/iGouJw2h3u1ro2CSNZwAgLifrqCjNiRN5Mq', 1, '01649671393', 'ZzlAvjfBhZjpg', 'Nt8ezUugfRS6woNTyTnV', 'wiPDo201ku', '56jmMT45uxtab2t90skex1Ywb0x6PX', NULL, NULL),
(64, '8UYe3XGqY3', 'nwFqhjLSzd9O@gmail.com', '$2y$10$mUQQWYmZikLvON0oqRZXquEjKkk.UTYIUK4d08smziTBIr.D4pDfK', 1, '01649671393', 'AQLgfNBD4Mjpg', 'MK9ro6zdDDmzhZxm6xY2', 'V4vHRjGxzy', 'dBhu2434P7nw3eIL4OJ1Hdon8fLiE7', NULL, NULL),
(65, 'awlVwNrbfR', 'QVepXOQRYAml@gmail.com', '$2y$10$qRePXKlumMeul9lio56tTOFizs8d/b6rBm59yUdAE0.XxBl31D22G', 1, '01649671393', '6QhH7GOCMrjpg', 'WHJOLr97I2Zv2I3Sl88h', 'CmkZzRj5yU', 'CVx83kZDjTZCi9AgDTrESNB7cRTwdE', NULL, NULL),
(66, 'GfXqJj55Jt', 'RCfzFNiIMuy7@gmail.com', '$2y$10$TFUJSSIZGGHx3cPK7NI9guWW7TzcjrXR7B9zJMCCs34YsH.8UrgOS', 1, '01649671393', 'kq3llY8Afejpg', '10YqaqsBzXcDFrSgi2X9', 'ASQ56CzqtJ', 'J81H0c5X1AXC3h8BWJ0uC1GfgzfYJj', NULL, NULL),
(67, '0UEegu9IXd', 'Ygbj4ROyi0tr@gmail.com', '$2y$10$5FzRcqjpl2UELY3DWhEVIuCxOY0KrXrrR9RtxQs.OsI3x6mecIVUi', 1, '01649671393', 'bO5mFkpw2djpg', 'DN3zf6jWKlEytvkAJDBi', 'p99TzMOxc3', 'MYwjUArjaaSt8GoIznQ1REpKqbUQ1B', NULL, NULL),
(68, 'jvpa7nCMzB', 'yFj46L5TdCVU@gmail.com', '$2y$10$dJ1fpACKnMOAXiHKvnlDvuY60FWkxhAaCLGjicEBmGC.XzIzZilxK', 1, '01649671393', 'FzQEyc5atwjpg', 'ty53b7r0oZDC4huC2Q2e', 'Y2rZpKkCO1', 'TCSh9LSeyDZ6rcY02BHgexjw1S2aRL', NULL, NULL),
(69, 'pRx0cfLJQo', '87PRSW35Kvvf@gmail.com', '$2y$10$Z5xDTpgHZL5Xsk2Vu1dWGecnVJqMALoLqMup/CfhJCtM0TCHLCaa.', 1, '01649671393', 'WnY3xfVBC2jpg', 'pYYB37fIsDqWtH8Hjyoa', 'fwMDjrnsYT', 'WTusje83GZJ9lfvd66R0AUI15uzf9q', NULL, NULL),
(70, '5QN7JbFOPo', 'e3wXBjBZYJEz@gmail.com', '$2y$10$zejanS/egW088pYvCM7Q1O6frRmC12f7otgIudRYVn4fiWKpQPHgm', 1, '01649671393', 'K7Oq9X4zS6jpg', 'guV7iI3qz2m0zAkqbyeC', 'W2EKgDIm7c', 'gT5VA8elpet8UI9QVJESx9OM8myK9G', NULL, NULL),
(71, 'HeAlSdwvyf', 'uhx2kHXIW9Hn@gmail.com', '$2y$10$PUmelBGbao6/2mMeXzOIwu2i63irMFkapwymeLjRgJChaKcsYmuLC', 1, '01649671393', 'Xp5wZH5RuZjpg', '8VsZrbVCnIHwAHjkd2m8', 'rfAOUMeFtN', 'dcg53VIwsK2G0UwMWsJFZW0Zxa9K9t', NULL, NULL),
(72, 'lSVSPQYOLq', 'mcqMe0aXNP8i@gmail.com', '$2y$10$66Sj7fTPlZbpI1jmT7vbN.8MS0jahEbRxW8Q/X9y154W8P4gfxaVC', 1, '01649671393', 'S3dR2ilSPAjpg', 'HUHf1D5qScK1cjcT2euO', '98zVvCQCfI', 'j0mVyr5dBLXVgacWNnSieZWove5Dh2', NULL, NULL),
(73, 'nd3ok9ZGp0', 'yacIVKUaNbeT@gmail.com', '$2y$10$ydTviYKmSOOnlghjk1PQ0Oj4fjqqHWAzDSZAoHnvqqu.fFN9zN9w2', 1, '01649671393', 'QmHXOX3bE0jpg', 'F0jsvQvdehqNbedPt9Fu', '7IlEzVOn1y', 'iJWuEMMO8HA9bC78FGoYz02InZ5MUA', NULL, NULL),
(74, 'F9gDCIcslw', 'ildTIbxCdEn7@gmail.com', '$2y$10$6yYLv1wRpiB5TzF4xvXmFeJoIvdm1ZXjM24KwsbMJZnuSEHl/4ur6', 1, '01649671393', 'PrEDs8I2wcjpg', 'Goab4ezKyMnmZjnVdb5g', 'olaz84GEhj', 'R40VsJfM3ofHPOChoIlZJpjJGEjIVC', NULL, NULL),
(75, 'nbZArDyCzw', 'WQIS9MhvGuMb@gmail.com', '$2y$10$IaUcJ6ox3wTczdw0XqxYQef4Qexb3I8eICsvROEffxpGBftqxjPpu', 1, '01649671393', 'GuJf4Fqh8yjpg', '2gJSiaJz0hdMruoCC6xC', '1wOYmB2xGe', 'bj1Fk27X3Bvn5LDMv7VS1BLzO1vBaR', NULL, NULL),
(76, 'B2smfEBKtV', 'zrJtBzr9ZtjD@gmail.com', '$2y$10$5AEjKvbXnw8vmJ8/No6tfel7iyleqlXwXNHg025RzO6hpyyx8NysS', 1, '01649671393', 's7cld1jUIQjpg', 'i92P7DMGVgXB462HfeLl', '1DFBSf9anr', 'NqhBWhMJSHB9DdKAtgZSIfscmSR8Le', NULL, NULL),
(77, 'Ymt90I5Bwb', 'gfWvl1UKbh7i@gmail.com', '$2y$10$1osmY5KPakFdvA4etH3EK.lFJNNyw42k9E9SzKN79jb1Z41lrYyXi', 1, '01649671393', 'CItOHob7mgjpg', 'Rtw9z5oND6U3nZIbBVIF', '6mxjjY5mWD', 'ahaZbos084NbGVwTPAzTeQzvopqwDh', NULL, NULL),
(78, 'ZQJKgw5BbS', 'DrWwZy8fkG74@gmail.com', '$2y$10$FgMyBjdOFCcRE6FIKSIot./mUgd1izABmwuhY0AJqQiUubL/wZRhS', 1, '01649671393', 'EzVxjnx34ljpg', 'gjmSUEIn67QyD3CBuWnQ', 'yXMalEUbXT', 'iXPryeM22AkJrsGQKfIS0YIi3i7gm0', NULL, NULL),
(79, '0Z0dX8sSzv', 'vxhi6y5kkYLp@gmail.com', '$2y$10$Vr3Xq36dUKCAblh0lCOQCuYHeAfufDb8ye79efVfxnZsy.Cjgvy52', 1, '01649671393', 'l6Zk1rYMfajpg', '5mBHp4A6oCZ7dnf6718w', '0jo5fJ7Lty', 'qKbcve7xd9hG0plNE3DIK7MGWob2cA', NULL, NULL),
(80, 'bBILp1BHlo', 'fhRKgYZ6dnVq@gmail.com', '$2y$10$K9y0pLNXvbahYx7jz/QLBezSVBBSrziE/L4uwH5sWFyQCetq6pgqO', 1, '01649671393', 'awF6TL6pRBjpg', '5b6JSx5yww5rNqzxfZ9U', 'nhAkM4N6oQ', 'D9XWIH4yR758DaF2zSilg34JlPJV77', NULL, NULL),
(81, 's8m3tCkKHT', 'zTUCgUGzpOoB@gmail.com', '$2y$10$s4PNlPBUhM6u4b9QrVj/gub3VMe7pFMyfg.RORFxtInS2uKSm7ZPm', 1, '01649671393', 'IbEuZ98aVvjpg', 'nXWXFXqhqK9QiccCGuW8', 'gyM8pxL90M', '421QgB8PCUF67vzb4e3xwvRO7M3x00', NULL, NULL),
(82, '6DrJPRIxWQ', 'sLlqx64igwPt@gmail.com', '$2y$10$ukL1ucy52anLa0KP8G5ZFO3.mgzMbO0pPdU2mu9FBoXN65l1h2PBe', 1, '01649671393', '2sHIuxnx0Cjpg', 'MIwDTSsb41Y4T8SB875i', 'XjVrofgsro', 'r1TiSHAMUpDFagymrXkpHMkTAScj2X', NULL, NULL),
(83, 'cnbV88VVeM', 'EncZIud6kugd@gmail.com', '$2y$10$VXIN71LCkKrbtiagDB2IVOyiJYcX.P9GCG.2cAHTRunXkaoKKBZwC', 1, '01649671393', 'bfWSoe1o2gjpg', 'N9tGVamcMfV5MRkdCWsU', 'Oo8EInmS8m', 'UCD3uUrSwzIBX573VpO3RU1DqeYLmr', NULL, NULL),
(84, 'L8Io5NtK0z', 'IIuprCCYKp9G@gmail.com', '$2y$10$48JDJyaHBspHVe47vZvK5.qE8kKTfHBvPDD.apJlpRXqcxlW2E1xu', 1, '01649671393', 'S5TMQyCYCcjpg', 'K38XKk4ZCyQRxOxGVKM8', 'BhfiUWNHzP', 'Yq4XjFN8P8gLXa3ejGgN1KpRVQrxoL', NULL, NULL),
(85, 'GthB292CtN', 'f0vTnrE8gfW1@gmail.com', '$2y$10$97TYidk1kCi1xjvklfHg9.y/JdPEF0Xrl36Dt.LAHr.rJdvUpBxt.', 1, '01649671393', 'DMzkNbXARGjpg', '90SKHj5D9SVflPXS9pwv', '2nLA5mpZxq', '0wSdWfczhHxCLNUooPRQLcJAPfmEHf', NULL, NULL),
(86, 'awkCzN6nYi', 'n3WTDwH1TXxG@gmail.com', '$2y$10$7XKIlumUN9UelwHscQ2p4.V0.OzmVfnLll5eFYFKl3I7to/yn406C', 1, '01649671393', 'seWYmUGrZ2jpg', 'aY6uO4mFg3rQ56c8r4m4', 'eMg2pkLALY', 'sm9GsPZlPUf1hFwCPabE0gUZiocbmi', NULL, NULL),
(87, 'PFVLLWZbbL', 'mrnpyftfg5Q3@gmail.com', '$2y$10$ADhF8AfOA0w..TyuIDsLIOwtz7Is.qG6dxaUfeZguBhFU3YlX1iaC', 1, '01649671393', 'iUMhieN4tljpg', 'lWVQa4OlsFRJ7nmdC0pw', 'lBLIC1SAMo', '3EoZlgKJOwzrbKalJFPbPbyOtWFneX', NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id_category`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password`
--
ALTER TABLE `password`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `phone`
--
ALTER TABLE `phone`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id_posts`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Chỉ mục cho bảng `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `phone`
--
ALTER TABLE `phone`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id_posts` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT cho bảng `role`
--
ALTER TABLE `role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category` (`id_category`),
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
