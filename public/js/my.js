$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function user_table(data,text,page,current_page,per_page)
    {
        var html='';
        html+= '<div class="col-lg-12">';
        html+= '<div class="panel panel-default">';
        html+= '<div class="panel-heading h1-1">';
        html+= '<h1>Danh Sách User</h1>';
        html+= '<input type="text" class="search" placeholder="Tìm Kiếm ">';
        if (text != undefined)
        {
            html+='<div><span style="float: left" class="btn btn-default" id="search1">Tìm kiếm</span>';
            html+='<span style="float: left;background-color: blue" class="btn btn-default" id="search2" >Tìm nâng cao</span></div>';
        }
        html+= '</div>';
        if (text != undefined)
        {
            html+='<div style="color: #1ab7ea;cursor:pointer;float: right" class="listUser btn btn-info">quay lại';
             html+='</div>';
            html+='<span>Kết quả cho tìm kiếm:';
            html+='<h4 id="searchAll" style="color: red">';
            html+=text;
            html+='<h4>';
            html+='</span>';
        }
        html+= '<div class="panel-body">';
        html+= '<div class="table-responsive">';
        html+= '<table class="table">';
        html+= '<thead>';
        html+= '<tr>';
        html+= '<th>STT</th>';
        html+= '<th> Tên</th>';
        html+= '<th>Email</th>';
        html+= '<th>Điện thoại</th>';
        html+= '<th>địa chỉ</th>';
        html+= '<th>ảnh đại diện</th>';
        html+= '<th style="cursor: pointer;" class="deleteUser "><i class="glyphicon glyphicon-trash"></i></th>';
        html+= '</tr>';
        html+= '</thead>';
        html+= '<tbody>';
        $.each (data, function (key, item)
        {
            console.log(item);
            html+='<tr>';
            html+='<td>';
            if (page == undefined)
            {
                html+=key+1;
            }else
            {
                html+=(((current_page*per_page)-per_page)+1)+key;
            }
            html+='</td>';
            html+='<td>';
            html+=item['name'];
            html+='</td>';
            html+='<td>';
            html+=item['email'];
            html+='</td>';
            html+='<td>';
            html+=item['phone'];
            html+='</td>';
            html+='<td>';
            html+=item['address'];
            html+='</td>';
            html+='<td><img style="width:60px" src="/image/';
            html+=item['avatar'];
            html+='" alt=""> </td>';
            html+='<td align="center"><input type="checkbox" class="i-checks " name="ids[]" value="';
            html+=item['id'];
            html+='"></td>';
            html+='<td class="glyphicon glyphicon-log-in detail" id="'+item['id']+'">';
            html+='</td>';
            html+='</tr>';
        });

        html+= '</tbody>';
        html+= '</table>';
        html+= '</div>';
        html+= '</div>';
        html+= '</div>';
        html+= '</div>';
        if (page != undefined)
        {
            for (var i=1; i<= page;i++)
            {
                if (i==current_page)
                {
                    html+='<button style="background-color: blue" class="page">'+i+'</button>';
                }
                if(i != current_page)
                {
                    html+='<button class="page">'+i+'</button>';
                }
            }
        }
        html+= '<tbody>';
        return html;
    }
    function user_table_search(data,text)
    {
        var html='';
        html+= '<div class="col-lg-12">';
        html+= '<div class="panel panel-default">';
        html+= '<div class="panel-heading h1-1">';
        html+= '<h1>Danh Sách User</h1>';
        html+= '<input type="text" class="search" placeholder="Tìm Kiếm ">';
        html+='<div><span style="float: left;background-color: blue" class="btn btn-default" id="search1">Tìm kiếm </span>';
        html+='<span style="float: left" class="btn btn-default" id="search2">Tìm kiếm nâng cao</span></div>';
        html+= '</div>';
        html+='<div style="color: #1ab7ea;cursor:pointer;float: right" class="listUser btn btn-info">quay lại';
        html+='</div>';
        html+='<span>Kết quả cho tìm kiếm:';
        html+='<h4 id="searchAll" style="color: red">';
        html+=text;
        html+='<h4>';
        html+='</span>';
        html+= '<div class="panel-body">';
        html+= '<div class="table-responsive">';
        html+= '<table class="table">';
        html+= '<thead>';
        html+= '<tr>';
        html+= '<th>STT</th>';
        html+= '<th> Tên</th>';
        html+= '<th style="cursor: pointer;" class="deleteUser "><i class="glyphicon glyphicon-trash"></i></th>';
        html+= '</tr>';
        html+= '</thead>';
        html+= '<tbody>';
        $.each (data, function (key, item)
        {
            console.log(item);
            html+='<tr>';
            html+='<td>';
            html+=key+1;
            html+='</td>';
            html+='<td>';
            html+=item['name'];
            html+='</td>';
            html+='<td align="center"><input type="checkbox" style="margin-left: -103px;" class="i-checks" name="ids[]" value="';
            html+=item['id'];
            html+='"></td>';
            html+='<td class="glyphicon glyphicon-log-in detail" id="'+item['id']+'">';
            html+='</td>';
            html+='</tr>';
        });
        html+= '</tbody>';
        html+= '</table>';
        html+= '</div>';
        html+= '</div>';
        html+= '</div>';
        html+= '</div>';
        html+= '<tbody>';
        return html;
    }
    $("form").on("submit", function (e) {
        e.preventDefault();
        var name= $('#name').val();
        var email=$('#email').val();
        var password= $('#password').val();
        var phone=$('#phone').val();
        var address= $('#address').val();
        var file = $('#avatar').prop('files')[0];
        $.ajax
        ({
            url:"/api/user/",
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success:function (data) {
                console.log(data);
                $('.errors').hide();
                if (data.errors == true)
                {
                    if (data[0].name != undefined)
                    {
                        $('.errorsname').show().text(data[0].name[0]);
                    }
                    if (data[0].email != undefined)
                    {
                        $('.errorsemail').show().text(data[0].email[0]);
                    }
                    if (data[0].password != undefined)
                    {
                        $('.errorspassword').show().text(data[0].password[0]);
                    }
                    if (data[0].phone != undefined)
                    {
                        $('.errorsphone').show().text(data[0].phone[0]);
                    }

                    if (data[0].address != undefined)
                    {
                        $('.errorsaddress').show().text(data[0].address[0]);
                    }
                    if (data[0].avatar != undefined)
                    {
                        $('.errorsavatar').show().text(data[0].avatar[0]);
                    }
                }else
                {
                    var html='<span id="';
                    html+=data.details.id
                    $('.success1').show().text('Thêm Mới Người Dùng Thành Công');
                    $('#submit').after(html);
                    $('#password').val('');
                }
            }
        })
    });

    $(document).on('click','.thongtin',function () {
       var id=$(this).attr('id');
        $.ajax
        ({
            'url':'/api/user/'+id,
            'type':'put',
            success:function (data) {
                console.log(data);
                alert('email:  '+ data.email+'     Mật Khẩu:   '+data.password);
            }
        })
    });

        $(document).on('click','.listUser',function () {
            $.ajax
            ({
                'url':'/api/user/',
                'type':'get',
                success:function (data) {
                    console.log(data);
                   a=user_table(data.data,undefined,data.last_page,data.current_page,data.per_page);
                    $('.alluser').html(a);
                }
            })
        });
    $(document).on('click','.page',function () {
        var page=$(this).text();
        $.ajax
        ({
            'url':'/api/user?page='+page,
            'type':'get',
            success:function (data) {
                console.log(data);
                a=user_table(data.data,undefined,data.last_page,data.current_page,data.per_page);
                $('.alluser').html(a);
            }
        })
    });

    $(document).on('click','.deleteUser',function () {
        var id=[];
        $('.i-checks:checked').each(function(i){
            id.push($(this).val());
        });
        console.log(id);
        var check=confirm('bạn có chắc chắn muốn xóa không?');
        if (check) {
            $.ajax({
                url: '/api/user/',
                type: 'delete',
                data: {'id':id},
                success: function (data) {
                    console.log(data);
                    a=user_table(data.data,undefined,data.last_page,data.current_page,data.per_page);
                    $('.alluser').html(a);
                }
            });
        }
    });
    $(document).on('change','.search',function () {
        var text= $('.search').val();
        $.ajax({
            url: '/api/search/',
            type: 'post',
            data: {'text':text},
            success: function (data) {
                console.log(data);
                var html=user_table_search(data,text);
                $('.alluser').html(html);
            }
        });
    });
    $(document).on('click','.detail',function () {
       var id=$(this).attr('id');
        $.ajax({
            url: '/api/user/'+id,
            type: 'put',
            data: {'id':id},
            success: function (data) {
                console.log(data);
               html='';
                html+='<div class="container-fluid">';
                html+='<div class="header">';
                html+='<div class="container headertop">';
                html+='<div class="row custombut">';
                html+='<nav class="navbar navbar-default">';
                html+='<div class="container-fluid">';
                html+='<!-- Brand and toggle get grouped for better mobile display -->';
                html+='<div class="navbar-header">';
                html+='<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">';
                html+='<span class="sr-only">Toggle navigation</span>';
                html+='<span class="icon-bar"></span>';
                html+='<span class="icon-bar"></span>';
                html+='<span class="icon-bar"></span>';
                html+='</button>';
                html+='<a class="navbar-brand" href="#">';
                html+='<img class="logoheader" style="width: 40px;height: 40px" src="image/lg.png" alt="">';
                html+='</a>';
                html+='</div>';
                html+='<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">';
                html+='<form class="navbar-form navbar-left">';
                html+='<div class="form-group">';
                html+='<input type="text" class="form-control" placeholder="Search">';
                html+='</div>';
                html+='</form>';
                html+='<ul class="nav navbar-nav navbar-right">';
                html+='<li><a href="#">';
                html+='<img class="avatar" src="image/'+data.avatar+'" alt="">';
                html+='</a></li>';
                html+='<li class=""><a href="#">'+data.name+ '</a></li>';
                html+='<li class=""><a href="#">Trang chủ </a></li>';
                html+='<li class=""><a href="#">Tạo </a></li>';
                html+='<li><a href="#"> <img src="image/kb.PNG" alt=""> </a></li>';
                html+='<li><a href="#"> <img src="image/tinnhan.PNG" alt=""> </a></li>';
                html+='<li><a href="#"> <img src="image/chuong.PNG" alt=""> </a></li>';
                html+='<li><a href="#"> <img src="image/addf.PNG" alt=""> </a></li>';
                html+='<li><a href="#"> <img src="image/support.PNG" alt=""> </a></li>';
                html+='<li class="dropdown">';
                html+='<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="caret"></span></a>';
                html+='<ul class="dropdown-menu">';
                html+='<li><a href="#">Action</a></li>';
                html+='<li><a href="#">Another action</a></li>';
                html+='<li><a href="#">Something else here</a></li>';
                html+='<li role="separator" class="divider"></li>';
                html+='<li><a href="#">Separated link</a></li>';
                html+='</ul>';
                html+='</li>';
                html+='</ul>';
                html+='</div>';
                html+='</div>';
                html+='</nav>';
                html+='</div>';
                html+='</div>';
                html+='</div>';
                html+='</div>';
                html+='<div class="container">';
                html+='<div class="row">';
                html+='<div class="content">';
                html+='<div style="position: relative;"  >';
                html+='<img class="banner" src="image/' +data.avatar+ '" alt="">';
                html+='<button class="banner-bottom1" >....</button>';
                html+='<button class="banner-bottom2 "><span style="padding:8px " class="glyphicon glyphicon-menu-hamburger"></span>Nhật kí hoạt động <span class="custom-alert">10</span></button>';
                html+='<button class="banner-bottom2 custom-buttom2 "style="position: absolute;">Cập nhật thông tin <span class="custom-alert">10</span></button>';
                html+='<img class="AvatarBanner" src="image/' +data.avatar+ '" alt="">';
                html+='<span class="Avatarname">'+data.name+'</span>';
                html+='</div>';
                html+='</div>';
                html+='</div>';
                html+='<div class="row">';
                html+='<div class="content">';
                html+='<div class="navbar-header">';
                html+='<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"></button>';
                html+='</div>';
                html+='<div class="collapse navbar-collapse buttombanner" id="bs-example-navbar-collapse-1">';
                html+='<ul class="nav navbar-nav navbar-right customnav">';
                html+='<li class="dropdown">';
                html+='<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dòng thời gian <span class="caret"></span></a>';
                html+='<ul class="dropdown-menu">';
                html+='<li><a href="#">Action</a></li>';
                html+='<li><a href="#">Another action</a></li>';
                html+='<li><a href="#">Something else here</a></li>';
                html+='<li role="separator" class="divider"></li>';
                html+='<li><a href="#">Separated link</a></li>';
                html+='</ul>';
                html+='</li>';
                html+='<li><a href="#">Giới thiệu</a></li>';
                html+='<li><a href="#">bạn bè</a></li>';
                html+='<li><a href="#">Ảnh</a></li>';
                html+='<li>';
                html+='<a style="float: left;"  href="#">Lưu Trữ';
                html+='<img class="pass" style="float: left;" src="image/pass.png" alt=""></a>';
                html+='</li>';
                html+='<li class="dropdown 	">';
                html+='<a href="#" class="dropdown-toggle customleft" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Xem Thêm <span class="caret"></span></a>';
                html+='<ul class="dropdown-menu">';
                html+='<li><a href="#">Action</a></li>';
                html+='<li><a href="#">Another action</a></li>';
                html+='<li><a href="#">Something else here</a></li>';
                html+='<li role="separator" class="divider"></li>';
                html+='<li><a href="#">Separated link</a></li>';
                html+='</ul>';
                html+='</li>';
                html+='</ul>';
                html+='</div>';
                html+='</div>';
                html+='</div>';
                html+='</div>';
                html+='</div>';
                html+='</div>';
                html+='</div>';
                $('#wrapper').html(html);
            }
        });
    });
    $(document).on('click','#search2',function () {
       var text=$('#searchAll').text();
        $.ajax({
            url: '/api/search/',
            type: 'post',
            data: {'text':text},
            success: function (data) {
                console.log(data);
                var a=user_table(data,text);
                $('.alluser').html(a);
            }
        });
    });
    $(document).on('click','#search1',function () {
        var text=$('#searchAll').text();
        $.ajax({
            url: '/api/search/',
            type: 'post',
            data: {'text':text},
            success: function (data) {
                console.log(data);
                var a=user_table_search(data,text);
                $('.alluser').html(a);
            }
        });
    });
});